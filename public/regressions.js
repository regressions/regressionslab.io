const applicationId = 'a09d08a8b356f76ef6fbdd2f3a25c6e9ee84214dae01772c144fe71ac2a58f66';

const teams = [
  'All',
  'backend',
  'Build',
  'CI',
  'Discussion',
  'Documentation',
  'Edge',
  'frontend',
  'Gitaly',
  'Platform',
  'UX'
];

function randomHex(length) {
  if (length > 0) {
    return Math.round(Math.random()*15).toString(16) + randomHex(length-1);
  }

  return '';
}

function login() {
  const oauthState = randomHex(64);
  localStorage.setItem('oauthState', oauthState);

  const currentPage = location.origin;
  location.href = `https://gitlab.com/oauth/authorize?client_id=${applicationId}&redirect_uri=${currentPage}&response_type=token&state=${oauthState}`;
}

function parseOAuthResponse(response) {
  if (!response || response === '') {
    return;
  }

  const oauthState = /state=([^&]+)/.exec(response)[1];
  const expectedOAuthState = localStorage.getItem('oauthState');
  localStorage.removeItem('oauthState');

  if (oauthState !== expectedOAuthState) {
    throw new Error('OAuth state mismatch!');
  }

  const errorDescription = /error_description=([^&]+)/.exec(response);
  if (errorDescription) {
    throw new Error(errorDescription[1].replace(/\+/g, ' '));
  }

  const accessToken = /access_token=([^&]+)/.exec(response)[1];
  localStorage.setItem('accessToken', accessToken);
  return accessToken;
}

function apiRequest(endpoint) {
  const accessToken = localStorage.getItem('accessToken');
  if (!accessToken) {
    return Promise.reject(new Error('Not logged in!'));
  }

  const headers = new Headers();
  headers.append('authorization', `Bearer ${accessToken}`);

  return fetch(`https://gitlab.com/api/v4${endpoint}`, { headers })
    .then(response => response.json()
      .then(jsonData => {
        if (!response.ok) {
          const error = new Error(jsonData.error || jsonData.message);
          error.status = response.status;
          throw error;
        }

        return jsonData;
      })
    );
}

function loadIssues(teamLabel) {
  const issuesList = document.querySelector('#issues-list');
  issuesList.innerText = 'Loading...';

  const labels = ['regression'];
  if (teamLabel) {
    labels.push(teamLabel);
  }

  const projects = ['gitlab-ce', 'gitlab-ee'];
  ;
  return Promise.all(
    projects.map(project =>
      apiRequest(`/projects/gitlab-org%2F${project}/issues?state=opened&labels=${labels.join(',')}`)
    )
  )
  .then(issues => {
    issuesList.innerText = '';

    issues = issues.reduce((a, b) => a.concat(b));
    issues.sort((a, b) => moment(a.updated_at).diff(b.updated_at));
    issues.forEach(issue => {
      const listItem = document.createElement('li');

      let assignee;
      if (!issue.assignees || !issue.assignees.length) {
        assignee = '<span class="avatar no-assignee"></span>';
      } else {
        assignee = issue.assignees[0];
        assignee = `<a href=${assignee.web_url}><img src="${assignee.avatar_url}" class="avatar"></a>`;
      }

      const project = (new RegExp('https://gitlab.com/gitlab-org/([^/]+)').exec(issue.web_url) || ['',''])[1];
      const lastUpdate = moment(issue.updated_at).fromNow();

      let lastUpdateColor = 'red';
      if (/hour/.test(lastUpdate)) {
        lastUpdateColor = '#F0AD4E';
      } else if (/(second|minute)/.test(lastUpdate)){
        lastUpdateColor = '#1f78d1';
      }

      listItem.innerHTML = `
        ${assignee}
        <a href="${issue.web_url}">${issue.title} (${project})</a>
        <span class="last-update" style="color: ${lastUpdateColor}">${lastUpdate}</span>
      `;

      issuesList.appendChild(listItem);
    });
  });
}

function selectTeam(teamLabel) {
  let url = location.origin;;
  if (teamLabel) {
    url += `?team=${teamLabel}`;
  }

  history.pushState('', teamLabel, url);

  const teamList = document.querySelector('#team-list');
  const buttons = teamList.querySelectorAll('button');
  for (var i = 0; i < buttons.length; i++) {
    buttons[i].classList.toggle('selected', buttons[i].dataset.teamLabel === teamLabel);
  }

  loadIssues(teamLabel);
}

function loadTeams() {
  const teamList = document.querySelector('#team-list');
  const clickHandler = event => selectTeam(event.target.dataset.teamLabel);
  teams.forEach(teamLabel => {
    const button = document.createElement('button');

    if (teamLabel !== 'All') {
      button.dataset.teamLabel = teamLabel;
    }

    button.onclick = clickHandler;
    button.innerText = teamLabel;
    teamList.appendChild(button);
  });
}

Promise.resolve(location.hash)
  .then(parseOAuthResponse)
  .then(() => {
    location.hash = '';
    return apiRequest('/user');
  })
  .then((user) => {
    document.querySelector('#current-user').innerHTML = `
      <img src="${user.avatar_url}" class="avatar">
      <a href="${user.web_url}">${user.name}</a>
    `;
  })
  .catch(error => {
    document.querySelector('#current-user').classList.add('hidden');
    document.querySelector('#login-message').classList.remove('hidden');
    document.querySelector('#login-button').onclick = login;

    throw error;
  })
  .then(loadTeams)
  .then(() => {
    const teamLabelMatch = /team=([^&]+)/.exec(location.search);
    selectTeam(teamLabelMatch && teamLabelMatch[1]);
  });
